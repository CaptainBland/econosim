import tables
import random
import sequtils
import sugar
import json
import algorithm
import tables
import strutils
import options
import os
import gnuplot

var
    X: seq[int] = @[0]
    Y = newSeqWith(len(X), random(10.0))
 
# cmd "set timefmt \"%Y-%m-%d\""
# cmd "set xdata time"

plot X, Y, "available listings"



# Number of companies in the simulation
let COMPANIES = 100
# How many listings they can start with (random range)
let MAX_START_LISTINGS = 2
let MIN_START_LISTINGS = 0

# These determine the probability that a resource is generated on a turn
let GEN_RESOURCES_NUMERATOR= 100
let GEN_RESOURCES_DIVISOR = 70

let ITEM_COST_MAX = 10.0
let ITEM_EFFECTIVENESS_MAX = 10.0

let START_MONEY_MAX = 1000.0

# External costs per item, e.g. VAT, maybe labour costs
let VAT = 3.0

# This is state
var LISTING_ID_COUNTER=0


randomize()

type
    NeedType* = enum
        A
        B
        C
        D

    Need* = object
        needKind*: NeedType
        value*: float64
    Company* = ref object of RootObj
        needs*: TableRef[NeedType, Need]
        money*: float64
        id*: int
        listings*: seq[Listing]

    Product* = object
        forNeeds*: Table[NeedType,float64]
    
    Listing* = object
        product*: Product
        price*: float64
        id*: int

proc printInfo(company: Company): string =
    echo "---------------------"
    let row = "$1\t$2\t$3\t$4"
    echo row % ["id", "money", "need", "listings"]
    let avgNeed = company.needs[NeedType.A].value
    echo row % [$company.id, formatFloat(company.money, format=ffDecimal, precision=2), formatFloat(avgNeed, format=ffDecimal, precision=2), $company.listings.len]


proc setupWorld*(): seq[Company] =
    var companies: seq[Company] = @[]
    for i in 0..COMPANIES:
        var listings: seq[Listing] = @[]
        for j in 0..rand(MAX_START_LISTINGS - MIN_START_LISTINGS) + MIN_START_LISTINGS:

            let product = Product(forNeeds: {NeedType.A: float64(rand(ITEM_EFFECTIVENESS_MAX))}.toTable)
            let listing = Listing(id: LISTING_ID_COUNTER, product: product, price: float64(rand(ITEM_COST_MAX)))
            LISTING_ID_COUNTER += 1
            listings.add(listing)

        let hunger = Need(needKind: NeedType.A, value: rand(1.0))
        let companyNeeds: TableRef[NeedType, Need] = {NeedType.A: hunger}.newTable
        let company = Company(id: i, listings: listings, needs: companyNeeds, money: rand(START_MONEY_MAX))
        companies.add(company)
    echo "Companies: ", companies.map(c => c.printInfo())

    return companies

proc flatten[T](input: seq[seq[T]]): seq[T] =
    var newSeq: seq[T] = @[]
    for subArr in input:
        for val in subArr:
            newSeq.add(subArr)
    return newSeq

proc findResource(buyer: Company, companies: seq[Company]): Option[(Company, Listing)] =
    #find the listing which fulfills the person's needs for the best value
    
    let sortedListings = companies
        .filter(c => c.listings.len > 0)
        #FIXME: work with multiple needs
        .map((seller: Company) => zip(
            [seller], 
            seller
                .listings
                .filter(s => s.product.forNeeds.hasKey(NeedType.A))))
        .flatten()
        .sorted((a,b) => a[1].product.forNeeds[NeedType.A] - b[1].product.forNeeds[NeedType.A])


    if(buyer.needs[NeedType.A].value <= 0):
        return none((Company, Listing))
    if(sortedListings.len > 0):
        return some(sortedListings[0])
    else: return none((Company, Listing))

proc acquireResource*(buyer: var Company, seller: var Company, listing: Listing) =
    if buyer.money < listing.price:
        echo "Buyer cannot afford purchase"
        return
        
    let product = listing.product

    seller.listings.keepIf(l => l.id != listing.id)

    for pair in product.forNeeds.pairs:
        let productNeed: NeedType = pair[0]
        let value = pair[1]
        if buyer.needs.hasKey(productNeed):
            buyer.needs[productNeed] = Need(needKind: productNeed, value: buyer.needs[productNeed].value - value)
    let vatAmount = listing.price * VAT
    buyer.money -= listing.price
    buyer.money -= vatAmount
    seller.money += listing.price

proc generateResources(company: var Company) =
    company.needs[NeedType.A] = Need(needKind: NeedType.A, value: company.needs[NeedType.A].value + rand(1.0))

    var genResource = rand(GEN_RESOURCES_NUMERATOR)
    if(genResource >= GEN_RESOURCES_DIVISOR):
        genResource = -1
    else:
        genResource = 0

    if company.needs[NeedType.A].value > 10:
        echo "Needs are too high"
        genResource = -1

    for i in 0..genResource:
        echo "Generating resource for $1", $company.id
        let product = Product(forNeeds: {NeedType.A: float64(rand(ITEM_EFFECTIVENESS_MAX))}.toTable)
        let listing = Listing(id: LISTING_ID_COUNTER, product: product, price: float64(rand(ITEM_COST_MAX)))
        LISTING_ID_COUNTER+=1
        company.listings.add(listing)

proc isEconomyCrashed(companies: seq[Company]): bool =
    let totalListings = foldl(companies.map(c => c.listings.len), a+b)
    if totalListings <= 0:
        echo "Resource collapse, nobody has any listings"
        return true
    
    let totalMoney = foldl(companies.map(c => c.money), a+b)
    if(totalMoney <= 0.01):
        echo "Nobody has any money"
        return true
    

proc runWorld(companies: var seq[Company]) =
    echo "runWorld"
    echo "====="
    var step = 0

    #echo companies
    var companiesTable = initTable[int, Company]()
    for company in companies:
        companiesTable.add(company.id, company)

    discard findResource(companies[0], companies)
    while true:
        sleep(100)
        X.add(step)
        Y.add((float)foldl(companies.map(c => c.listings.len), a + b))

        plot X, Y

        if(isEconomyCrashed(companies)):
            echo "Economy ran for $1 turns" % $step
            return
            
        step += 1
        echo "Step: ", step
        for buyer in companies:
            var tempBuyer = buyer
            generateResources(tempBuyer)
            echo buyer.printInfo
            let maybeResource = buyer.findResource(companies)
            if(maybeResource.isSome()):
                var (company, listing) = maybeResource.get()
                acquireResource(tempBuyer, company, listing)


when isMainModule:
    var res = setupWorld()
    runWorld(res)