import unittest
import nimsim
import random
import tables
import math

doAssert(1 + 1 == 2)

suite "Basic simulation tests":
    echo "Suite setup for simulation tests..."
    
    setup:
        echo "setup"
    
    test "weird loopy thing":
        let val = -1
        for i in 0..val:
            echo "loopy thing!11!!1!!!"

    test "acquireResource test":
        echo "blah"
        let companies = setupWorld()
        var buyer = companies[0]
        var seller = companies[1]

        var resource = seller.listings[0]

        var initialNeed = buyer.needs[NeedType.A].value
        var initialBuyerMoney = buyer.money
        var initialSellerMoney = seller.money

        echo "buyer: ", initialBuyerMoney
        echo "seller: ", initialSellerMoney
        echo "price: ", resource.price

        acquireResource(buyer, seller, resource)

        doAssert(seller.listings[0].product.forNeeds[NeedType.A] != initialNeed)
        
        echo "buyer after: ", buyer.money
        doAssert(abs(buyer.needs[NeedType.A].value - (initialNeed - resource.product.forNeeds[NeedType.A])) < 0.01)
        doAssert(abs(buyer.money - (initialBuyerMoney - resource.price)) < 0.01)
        doAssert(abs(seller.money - (initialSellerMoney + resource.price)) < 0.01)
        

