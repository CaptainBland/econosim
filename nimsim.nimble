# Package

version       = "0.1.0"
author        = "Captain Bland"
description   = "simple simulations in nim"
license       = "GPLv3"
srcDir        = "src"
bin           = @["nimsim"]

# Dependencies

requires "nim >= 0.18.0"
requires "gnuplot"